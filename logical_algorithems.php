<!-- 1. Write a program that prints the numbers from 1 to 100 and for multiples of '3' print "Fizz" instead of the number and for the multiples of '5' print "Buzz". -->

<?php

// php program to print fizz buzz
$i;
for ($i=1; $i<=100; $i++) {

	// number divisible by 3 and 
    // 5 will always be divisible  
    // by 15, print 'FizzBuzz' in  
    // place of the number 
	if($i % 15 == 0)
		echo "FizzBuzz" . " ";
	
	// number divisible by 3? print  
    // 'Fizz' in place of the number 
	else if ($i % 3 == 0) {
	 	echo "Fizz" . " ";
	 }

	 // number divisible by 5, print  
    // 'Buzz' in place of the numbe
    else if ($i % 5 == 0) {
     	echo "Buzz" . " ";
     } 

     // print the number 
     else echo $i, "<br/>";
}
?>
===============
<!-- Output -->
================

<!-- 1
2
Fizz 4
Buzz Fizz 7
8
Fizz Buzz 11
Fizz 13
14
FizzBuzz 16
17
Fizz 19
Buzz Fizz 22
23
Fizz Buzz 26
Fizz 28
29
FizzBuzz 31
32
Fizz 34
Buzz Fizz 37
38
Fizz Buzz 41
Fizz 43
44
FizzBuzz 46
47
Fizz 49
Buzz Fizz 52
53
Fizz Buzz 56
Fizz 58
59
FizzBuzz 61
62
Fizz 64
Buzz Fizz 67
68
Fizz Buzz 71
Fizz 73
74
FizzBuzz 76
77
Fizz 79
Buzz Fizz 82
83
Fizz Buzz 86
Fizz 88
89
FizzBuzz 91
92
Fizz 94
Buzz Fizz 97
98
Fizz Buzz -->


<!-- 2. Printing Pyramid Pattern(Left-Side angle) -->

<?php

for ($i=1; $i<=5; $i++) {
    
    for ($j=1; $j <= $i; $j++) { 
        
        echo "*";
    }

    echo "<br/>";
}

?>

===============
<!-- Output -->
===============

<!-- 
*
**
***
****
***** 
-->